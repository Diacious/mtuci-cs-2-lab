# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:
# Hangman Game

import random

WORDLIST_FILENAME = "words.txt"


def load_words():
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist




def choose_word(wordlist):
    return random.choice(wordlist)

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()




def is_word_guessed(secret_word, letters_guessed):
    return len( [ch for ch in secret_word.lower() if (ch in letters_guessed)] ) == len(secret_word)




def get_guessed_word(secret_word, letters_guessed):
    word = secret_word
    vowels = 'aeiouy'

    if not(letters_guessed):     #условие создано на момент первого ввода буквы(когда фактически ничего не передается функциям)
        return '_ '*len(secret_word)

    if letters_guessed[-1] in secret_word:
        comment = 'Good guess'
        k = 1    # k сожержит все увеличение или уменьшение количества попыток
    else:
        comment = 'Ohh, this letter isnt in my word'
        k = -1


    for ch in secret_word:
        if not(ch in letters_guessed):
            word = word.replace(ch, '_ ')

    if not(letters_guessed[-1] in secret_word) and (letters_guessed[-1] in vowels):
        k = -2    # если введенная буква отсутствует в слове и она гласная
                  # то попытки сокращаюьтся на 2
    return comment,word,k




def get_available_letters(letters_guessed,warnings):
    english_letters = 'abcdefghijklmnopqrstuvwxyz'
    if not(letters_guessed):    # условие создано на момент первого ввода буквы(когда фактически ничего не передается функциям)
        return english_letters,english_letters



    last_word = get_guessed_word(secret_word, letters_guessed[:-1])[1] # переменная содержит значение get_guessed_word от предыдушего раза
    letter = letters_guessed[-1]  # текущая введеная буква

    # цикл, который проперяет на введеный недопустимый символ
    # или на повторно введеную букву
    while (letter in letters_guessed[:-1]) or (not(letter in english_letters)):
        warnings -=1
        if warnings == 0:
            print("You now have",warnings ," warnings:")
            print(last_word)
            return False
        if not(letter in english_letters):
            print('Oops! That is not a valid letter. You have', warnings, ' warnings left: ', last_word)
        else:
            print("Oops! You've already guessed that letter. You now have", warnings," warnings :",last_word)
        letter = input('Введите букву').lower()

    #цикл выводит строку неиспользованных букв
    for ch in letters_guessed:
        if english_letters.find(ch) != 0:
                english_letters = english_letters.replace(ch,'')

    return english_letters,letter,warnings

    
    

def hangman(secret_word):
    print('I am thinking of a word that is', len(secret_word), 'letters long.')
    letters_guessed = []
    print(secret_word)
    print('You have 3 warnings left.')
    k = 6    # количество попыток
    w = 3    # количество предупреждений

    # количество уникальных букв в загаданном слове
    unique_letters = len([x for x in range(len(secret_word))
                          if not(secret_word[x] in secret_word[x+1:])])
    while k>0:
        print('You have', k,' guesses left.')
        print('Available letters: ',get_available_letters(letters_guessed,w)[0],)
        letters_guessed.append(input('Please guess a letter: ').lower())
        turn = get_available_letters(letters_guessed,w)

        if not(turn):
            print("You've got too many warnings")
            letters_guessed.pop(-1)
            w = 3
            k -= 1
        else:
            # Если выолняется  требование для вводимых букв,то идёт вызов функции get_guessed_word
            letters_guessed.pop(-1)
            letters_guessed.append(turn[1])
            guess_word = get_guessed_word(secret_word, letters_guessed)
            print(guess_word[0],guess_word[1])    # выводит результат от ввода новой буквы и частично заполненное слово

            if is_word_guessed(secret_word, letters_guessed):
                print('Congratulations, you won!')
                print('Your total score for this game is: ', k * unique_letters)
                break

            k += guess_word[2]    # увеличивается/ууменьшается количество попыток в зависимости от введеной буквы
            if k>6:
                k = 6
            w = turn[2]
    if k <=0:
        print("You've lost. The secret word was : ",secret_word)




def match_with_gaps(my_word, other_word):
    if len(my_word) != len(other_word):
        return False
    else:
        for i in range(len(my_word)):    # в цикле происходит сопостовление частично отгаданно слово с загаданным словом
            ch_1 = my_word[i]
            ch_2 = other_word[i]
            if ((ch_1 != ch_2) or (my_word.count(ch_1) != other_word.count(ch_2))) and (ch_1 != '_'):
                return False
        return other_word





def show_possible_matches(my_word):
    res = []

    for other_word in wordlist:
        result = match_with_gaps(my_word,other_word)
        if result:
            res.append(other_word)
    if len(res)>0:
        print(res)
    else:
        print('No words was found')




def hangman_with_hints(secret_word):
    print('I am thinking of a word that is', len(secret_word), 'letters long.')
    letters_guessed = []
    print(secret_word)
    print('You have 3 warnings left.')
    k = 6   # количество попыток
    w = 3   # количество предупреждений
    guess_word = get_guessed_word(secret_word,'')

    # количество уникальных букв в загаданном слове
    unique_letters = len( [x for x in range(len(secret_word))
                          if not (secret_word[x] in secret_word[x + 1:])] )
    while k > 0:
        print('You have', k, ' guesses left.')
        print('Available letters: ', get_available_letters(letters_guessed, w)[0])
        letters_guessed.append(input('Please guess a letter: ').lower())

        if letters_guessed[-1] == '*':
            letters_guessed.pop(-1)
            show_possible_matches(guess_word[1].replace(' ',''))

        else:
            turn = get_available_letters(letters_guessed, w)
            if not (turn):
                print("You've got too many warnings")
                letters_guessed.pop(-1)
                w = 3
                k -= 1
            else:
                # Если выолняется  требование для вводимых букв,то идёт вызов функции get_guessed_word
                letters_guessed.pop(-1)
                letters_guessed.append(turn[1])
                guess_word = get_guessed_word(secret_word, letters_guessed)
                print(guess_word[0], guess_word[1])     # выводит результат от ввода новой буквы и частично заполненное слово

                if is_word_guessed(secret_word, letters_guessed):
                    print('Congratulations, you won!')
                    print('Your total score for this game is: ', k * unique_letters)
                    break

                k += guess_word[2]    # увеличивается/ууменьшается количество попыток в зависимости от введеной буквы
                if k > 6:
                    k = 6
                w = turn[2]
        if k <= 0:
            print("You've lost. The secret word was : ", secret_word)

    
#secret_word = choose_word(wordlist)
#hangman(secret_word)


secret_word = choose_word(wordlist)
hangman_with_hints(secret_word)
